---
title: tomcat 调优
---

查看tomcat 8080端口的tcp连接数
> netstat -an| grep 8080 |awk '/^tcp/{++S[$NF]}END{for (a in S)print a,S[a]}'

可以查看连接数量，如下图：
![tomcat-tcp](/images/tomcat-tcp-count.png)

现在的业务场景是多客户端，短连接，每个请求的处理时间相对较短，需要较多的线程来处理。相应的需要调优tomcat的并发参数，centos系统的参数调整暂不讨论
tomcat的并发参数通过调整server.xml里面的连接和线程池来获取更大的吞吐能力,一般是调整executor,connector组件
下面的参数都是我自己的理解，可能会有出入，请参考官方英文文档
> https://tomcat.apache.org/tomcat-8.5-doc/config/service.html

Executor component

| 参数名称 |类型| 说明 |
|:--------|--------|--------|
|className|string|继承类org.apache.catalina.Executor接口的线程池额执行类，可以自己实现|
|name|string|关联server.xml 其他的定义好的executor|
|threadPriority|int|executor的线程优先级，默认值是5(Thread.NORM_PRIORITY)|
|daemon|booean|是否开启守护线程|
|namePrefix|string|定义线程的头部名称  最后线程名称是namePrefix + threadNumber|
|maxThreads|int|最大线程数，默认值200|
|minSpareThreads|int|核心线程数的数量，默认值是25|
|maxIdleTime|int|空闲线程的关闭事件，单位是ms，如果是活跃线程数量小于或者等于核心线程数不会关闭空闲线程，默认值60000ms|
|maxQueueSize|int|队列最大数值，默认值是int的最大值，当队列满时，提供拒绝策略|
|prestartminSpareThreads|boolean|是否在启动executor时，启动核心线程数量，默认值是false|
|threadRenewalDelay|long|如果ThreadLocalLeakPreventionListener有配置，在恢复线程池里面线程时，未了避免线程同时创建卡住的情况，两个线程之间的间隔时间，默认值是1000ms，设置为负值时，线程不会被重建|

Connector HTTP/1.1 component

| 参数名称 |类型| 说明 |
|:--------|--------|--------|
|allowTrace|boolean|是否支持http trace方法|
|asyncTimeout|int|异步请求的的超时时间，默认是30000ms|
|defaultSSLHostConfigName|string|默认SSLHostConifg的名称|
|enableLookups|boolean|设置为true返回远程机器的hostName，设置为false返回ip地址|
|maxHeaderCount|int|default is 100，允许的最大的header里面条目的条数|
|maxParameterCount|int|默认值是10000，get参数+post参数的最大参数|
|maxPostSize|int|默认值是2097152byte（2M），设置post的最大字节数|
|maxSavePostSize|int|在FORM or CLIENT-CERT authentication认证的时候，post的大小，默认是4096byte（4kb）|
|parseBodyMethods|string array|解析哪些请求method到POST方式，默认是POST，可以设置PUT method -- 应该是兼容PUT模式时使用|
|port|int|端口号|
|protocol|协议|默认值是HTTP/1.1 限制支持三种 Http11NioProtocol，Http11Nio2Protocol，Http11AprProtocol |
|proxyName|string|代理名称|
|proxyPort|int|代理端口号|
|redirectPort|int|如果是https 请求8080端口时，强制跳转https端口|
|scheme|string|默认值是http|
|secure|boolean|标记请求是安全的，调用request.isSecure 返回true|
|sendReasonPhrase|boolean|为true时，会在返回中加上原因|
|URIEncoding|boolean|true:ISO-8859-1 false:UTF-8|
|useBodyEncodingForURI|boolean|false:ISO-8859-1  true:UTF-8|
|useIPVHosts|boolean|为true使用ip address作为绑定|
|xpoweredBy|boolean|使用头部建议，例如 x-power-by:openresty/1.13.6.1|
|acceptCount|int|如果所有服务于连接的线程都已经被使用了，连接无可用线程时，会存放到这个队列里面，默认值是100|
|acceptorThreadCount|int|默认值是1，如果是多核CPU需要增加这个值，如果有很多非长连接时，通常需要增加这个值|


-- 未完待续


















