---
title: JAVA scatter
---

查看系统的线程上下文切换次数

使用vmstat 3
![vmstat](https://db101.cn/images/TIM截图20180704163235.png)

其中cs就是context switch的次数，数值越高代表上线文切换的越频繁，耗时越多。每秒的切换次数可以和进程的总线程数进行比对，如果大于线程总数代表是满负荷运转

_ _ _

java 线程状态之间的跃迁
![thread change](https://db101.cn/images/TIM截图20180704193005.png)

线程的各个状态及说明如下：

| 状态名称 | 说明 |
|:--------|--------|
|    NEW    |     初始状态   |
|    RUNNABLE |  运行状态      |
|    BLOCKED    | 阻塞状态       |
|    WAITING    |等待状态|
|    TIME_WAITING    |  超时等待状态      |
|    TERMINATED    |终止状态        |

_ _ _

synchronized 关键字的使用场景

|锁的方式|锁的对象|伪代码|
|:--------|--------|--------|
|实例方法|类的实例对象|public synchronized void method|
|静态方法|类对象|public synchronized static void method|
|实例对象|类的实例对象|synchronized (this)|
|class对象|类对象|synchronized (A.class)|
|普通对象|普通对象|synchronized (o)|

_ _ _

mark 下 通过ReentrantLock中线程等待唤醒的状态跃迁
![await|singal](https://db101.cn/images/TIM截图20180720114120.png)

_ _ _

java queue 方法

| 方法名称 | 说明 | 是否阻塞 | 是否抛出异常 |
|:--------|--------|--------|--------|
|add|插入|否|是|
|offer|插入|否|是|
|remove|删除|否|否|
|poll|删除|否|否|
|element|获取|否|是|
|peek|获取|否|是|

blocking queue

| 方法名称 | 说明 | 是否阻塞 | 是否抛出异常 |
|:--------|--------|--------|--------|
|put|插入|是|否|
|offer *支持超时* |插入|是|否|
|take|删除|是|否|
|poll *支持超时* |删除|是|否|

命令行显示乱码：

```
修改cmd窗口字符编码为UTF-8，命令行中执行：chcp 65001

切换回中文GBK：chcp 936

剩余细节如改变字体请自行百度“命令行不能新显示中文”即可

```

windows中查看端口情况


```
netstat -ano | findstr 6379
```

mysql 查看binlog

```
mysqlbinlog --start-date="2019-05-27 16:31:00" --stop-date="2019-05-27 16:32:00"  -vv --base64-output=decode-rows /opt/mysql-bin.000787 | more

```

interrupt
设置中断标志位
![thread change](https://db101.cn/images/TIM截图20190605114040.png)

jenkins远程发布脚本

```
source /etc/profile
APP_HOME=~/app
TARGET_DIR=$APP_HOME/webapps/ROOT
DEPLOY_HOME=~/deploy
SRC_PATH="$DEPLOY_HOME/src"
ARCHIVE_PATH="$DEPLOY_HOME/archive"
BACKUP_PATH="$DEPLOY_HOME/backup"
echo "DEPLOY HOME: $DEPLOY_HOME"
echo "TARGET_DIR: $TARGET_DIR"
if [ ! -d "$TARGET_DIR" ]; then
echo "The directroy $TARGET_DIR is not found."
exit 1
fi
if [ -d "$ARCHIVE_PATH" ]; then
if [ -d "$SRC_PATH" ]; then
rm -rf $SRC_PATH/**
else
mkdir $SRC_PATH
fi
unzip $ARCHIVE_PATH/*.war -d $SRC_PATH/
cd $TARGET_DIR
files=`ls`
if [ ! -z "$files" ]; then
if [ ! -d "$BACKUP_PATH" ]; then
mkdir $BACKUP_PATH
fi
echo "Backup files."
t=$(date +%Y%m%d%H%M%S)
tar -cvf $BACKUP_PATH/$t.tar ./**
rm -rf ./**
fi
cp -rf $SRC_PATH/** ./
rm -rf $SRC_PATH/**
rm -rf $ARCHIVE_PATH/**
echo "Deploy success!"
fi
echo "Restarting tomcat"
$APP_HOME/bin/restartup.sh
exit 0
```

maven 父子工程项目编译脚本
```
clean compile -pl server-web -am -U package -P prod -DskipTests
```



