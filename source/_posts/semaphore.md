---
title: 并发工具类
---



#### Semaphore



是一个分离的信号量计数器

主要核心方法有两个：

*	acquire

*从信号队列里面获取一个信号量，如果没有获取到则会一直阻塞到获取到信号量为止*

*	release

*释放一个信号量回队列，这种模型相对来说比较适合池这种类型的业务需求*



acquire(int permits) 一次获取多个信号量

acquireUninterruptibly() 连续获取一个信号量

acquireUninterruptibly(int permits) 连续获得多个信号量



适合连接池的数量控制，维护一个池的总量



#### CountDownLatch

*	await() throws InterruptedException 调用该方法的线程等到构造方法传入的N减到0的时候，才能继续往下执行

*	await(long timeout, TimeUnit unit) 超时设置

*	countDown() 计数器减1

*	getCount()



适合等待一堆线程完成任务后，在执行主线程任务。



#### CyclicBarrier

* 	await() 使异步线程等待

* 	await(long timeout, TimeUnit unit) 超时设置

* 	getNumberWaiting() 返回哪些异步线程已经处于等待状态

* 	isBroken() 查询等待线程是否已经被中断

* 	reset() 如果所有线程都处于等待状态，执行完后续的操作后，可以重置 继续使用



除了默认构造方法，还可以传入一个Runnable任务，执行

> public CyclicBarrier(int parties, Runnable barrierAction)



可以执行一些回调方法

上面countDownLatch的功能也可以用这个来实现，在异步线程的最后面调用await方法，可以做到让异步线程同时开始一件事情











